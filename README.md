# Welcome to APIbuilder 👋

> A Symfony first attempt : creating a RESTFul API

## Install

```
npm install
```


## Author

👤 **Manon AYOU , Pauline LOPEZ**

* Gitlab: [@manon.ayou , @paulinelopez31650](https://gitlab.com/manon.ayou , https://gitlab.com/paulinelopez31650)
* LinkedIn: [@manon-ayou , @pauline-lopez](https://linkedin.com/in/manon-ayou , https://linkedin.com/in/pauline-lopez)

## Special thanks

_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_