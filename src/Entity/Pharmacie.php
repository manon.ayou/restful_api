<?php

namespace App\Entity;

use App\Repository\PharmacieRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PharmacieRepository::class)
 */
class Pharmacie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Pharmacie;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPharmacie(): ?string
    {
        return $this->Pharmacie;
    }

    public function setPharmacie(string $Pharmacie): self
    {
        $this->Pharmacie = $Pharmacie;

        return $this;
    }
}
